﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FaqV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractFaqServices abstractFaqServices;
        #endregion

        #region Cnstr
        public FaqV1Controller(AbstractFaqServices abstractFaqServices)
        {
            this.abstractFaqServices = abstractFaqServices;
        }
        #endregion

        //Faq_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Faq_Delete")]
        public async Task<IHttpActionResult> Faq_Delete(int Id, int DeletedBy)
        {
            var quote = abstractFaqServices.Faq_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Faq_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Faq_ById")]
        public async Task<IHttpActionResult> Faq_ById(int Id)
        {
            var quote = abstractFaqServices.Faq_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Faq_Upsert Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Faq_Upsert")]
        public async Task<IHttpActionResult> Faq_Upsert(Faq faq)
        {
            var quote = abstractFaqServices.Faq_Upsert(faq);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
      
       
        [System.Web.Http.HttpPost]
        [InheritedRoute("Faq_All")]
        public async Task<IHttpActionResult> Faq_All(PageParam pageParam, string search = "", int ForFaq=0)
        {
            var quote = abstractFaqServices.Faq_All(pageParam, search, ForFaq);
            return this.Content((HttpStatusCode)200, quote);
        }

        
    }
}
