﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HelpV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractHelpServices abstractHelpServices;
        #endregion

        #region Cnstr
        public HelpV1Controller(AbstractHelpServices abstractHelpServices)
        {
            this.abstractHelpServices = abstractHelpServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Help_All")]
        public async Task<IHttpActionResult> Help_All(PageParam pageParam, string search = "")
        {
            var quote = abstractHelpServices.Help_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        
    }
}
