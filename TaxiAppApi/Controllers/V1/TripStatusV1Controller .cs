﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TripStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTripStatusServices abstractTripStatusServices;
        #endregion

        #region Cnstr
        public TripStatusV1Controller(AbstractTripStatusServices abstractTripStatusServices)
        {
            this.abstractTripStatusServices = abstractTripStatusServices;
        }
        #endregion

        

       
        //TripStatus_All Api

        
        [System.Web.Http.HttpPost]
        [InheritedRoute("TripStatus_All")]
        public async Task<IHttpActionResult> TripStatus_All(PageParam pageParam, string search = "")
        {
            AbstractTripStatus TripStatus = new TripStatus();

            var quote = abstractTripStatusServices.TripStatus_All(pageParam, search, TripStatus);
            return this.Content((HttpStatusCode)200, quote);
        }

        
    }
}
