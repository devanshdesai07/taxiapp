﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractUserTypeServices abstractUserTypeServices;
        #endregion

        #region Cnstr
        public UserTypeV1Controller(AbstractUserTypeServices abstractUserTypeServices)
        {
            this.abstractUserTypeServices = abstractUserTypeServices;
        }
        #endregion
     
        //Users_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("UserType_All")]
        public async Task<IHttpActionResult> UserType_All(PageParam pageParam, string search = "")
        {
            AbstractUserType UserType = new UserType();
            var quote = abstractUserTypeServices.UserType_All(pageParam, search, UserType);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
