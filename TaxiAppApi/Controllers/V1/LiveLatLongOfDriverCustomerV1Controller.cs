﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LiveLatLongOfDriverCustomerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLiveLatLongOfDriverCustomerServices abstractLiveLatLongOfDriverCustomerServices;
        #endregion

        #region Cnstr
        public LiveLatLongOfDriverCustomerV1Controller(AbstractLiveLatLongOfDriverCustomerServices abstractLiveLatLongOfDriverCustomerServices)
        {
            this.abstractLiveLatLongOfDriverCustomerServices = abstractLiveLatLongOfDriverCustomerServices;
        }
        #endregion
        [System.Web.Http.HttpPost]
        [InheritedRoute("LiveLatLongOfDriver_Upsert")]
        public async Task<IHttpActionResult> LiveLatLongOfDriver_Upsert(LiveLatLongOfDriverCustomer liveLatLongOfDriverCustomer)
        {
            var quote = abstractLiveLatLongOfDriverCustomerServices.LiveLatLongOfDriver_Upsert(liveLatLongOfDriverCustomer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Users_All Api
        //Trip_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("livelatitude_By_CustomerId")]
        public async Task<IHttpActionResult> livelatitude_By_CustomerId(PageParam pageParam, string search = "", int CustomerId = 0, int TripId = 0)
        {
            AbstractLiveLatLongOfDriverCustomer LiveLatLongOfDriverCustomer = new LiveLatLongOfDriverCustomer();
            LiveLatLongOfDriverCustomer.CustomerId = CustomerId;
            LiveLatLongOfDriverCustomer.TripId = TripId;

            var quote = abstractLiveLatLongOfDriverCustomerServices.livelatitude_By_CustomerId(pageParam, search, LiveLatLongOfDriverCustomer);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("livelatitude_By_driverId")]
        public async Task<IHttpActionResult> livelatitude_By_driverId(PageParam pageParam, string search = "", int DriverId = 0, int TripId = 0)
        {
            AbstractLiveLatLongOfDriverCustomer LiveLatLongOfDriverCustomer = new LiveLatLongOfDriverCustomer();
            LiveLatLongOfDriverCustomer.TripId = TripId;
            LiveLatLongOfDriverCustomer.DriverId = DriverId;
           
            var quote = abstractLiveLatLongOfDriverCustomerServices.livelatitude_By_driverId(pageParam, search, LiveLatLongOfDriverCustomer);
            return this.Content((HttpStatusCode)200, quote);
        }
       
    }
}
