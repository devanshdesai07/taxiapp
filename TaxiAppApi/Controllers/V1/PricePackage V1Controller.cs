﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PricePackageV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPricePackageServices AbstractPricePackageServices;
        #endregion

        #region Cnstr
        public PricePackageV1Controller(AbstractPricePackageServices abstractPricePackageServices)
        {
            this.AbstractPricePackageServices = abstractPricePackageServices;
        }
        #endregion
        //PricePackage_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("PricePackage_ById")]
        public async Task<IHttpActionResult> PricePackage_ById(int Id)
        {
            var quote = AbstractPricePackageServices.PricePackage_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("PricePackageAPI_All")]
        public async Task<IHttpActionResult> PricePackageAPI_All(PageParam pageParam, string Search = "")
        {
            AbstractPricePackage PricePackage = new PricePackage();
            var quote = AbstractPricePackageServices.PricePackageAPI_All(pageParam, Search, PricePackage);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
