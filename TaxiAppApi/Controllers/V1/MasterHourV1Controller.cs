﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterHourV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterHourServices AbstractMasterHourServices;
        #endregion

        #region Cnstr
        public MasterHourV1Controller(AbstractMasterHourServices abstractMasterHourServices)
        {
            this.AbstractMasterHourServices = abstractMasterHourServices;
        }
        #endregion
        
        //AdminUsers_All Api

        //Users_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminUsers_All")]
        public async Task<IHttpActionResult> MasterHour_All(PageParam pageParam)
        {
            AbstractMasterHour MasterHour = new MasterHour();
            var quote = AbstractMasterHourServices.MasterHour_All(pageParam, MasterHour);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
