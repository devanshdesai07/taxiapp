﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerServices abstractCustomerServices;
        private readonly AbstractCustomerNotificationsServices abstractCustomerNotificationsServices;
        #endregion

        #region Cnstr
        public CustomerV1Controller(AbstractCustomerServices abstractCustomerServices,AbstractCustomerNotificationsServices abstractCustomerNotificationsServices)
        {
            this.abstractCustomerServices = abstractCustomerServices;
            this.abstractCustomerNotificationsServices = abstractCustomerNotificationsServices;
        }
        #endregion

        //Customer_ActInact Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_ActInAct")]
        public async Task<IHttpActionResult> Customer_ActInAct(int Id)
        {
            var quote = abstractCustomerServices.Customer_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Customer_ActInact Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_RcCardApproved")]
        public async Task<IHttpActionResult> Customer_RcCardApproved(int Id)
        {
            var quote = abstractCustomerServices.Customer_RcCardApproved(Id);
            if (quote.Item != null && quote.Code == 200)
            {
                AbstractCustomerNotifications abstractCustomerNotifications = new CustomerNotifications();
                abstractCustomerNotifications.CustomerId = quote.Item.Id;
                abstractCustomerNotifications.Message = "Your Rc Card Approved Successfully";

                abstractCustomerNotificationsServices.CustomerNotifications_Upsert(abstractCustomerNotifications);
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Customer_ActInact Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_IdProofApproved")]
        public async Task<IHttpActionResult> Customer_IdProofApproved(int Id)
        {
            var quote = abstractCustomerServices.Customer_IdProofApproved(Id);
            if (quote.Item != null && quote.Code == 200)
            {
                AbstractCustomerNotifications abstractCustomerNotifications = new CustomerNotifications();
                abstractCustomerNotifications.CustomerId = quote.Item.Id;
                abstractCustomerNotifications.Message = "Your Id Proof Approved Successfully";

                abstractCustomerNotificationsServices.CustomerNotifications_Upsert(abstractCustomerNotifications);
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Customer_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_ById")]
        public async Task<IHttpActionResult> Customer_ById(int Id)
        {
            var quote = abstractCustomerServices.Customer_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_Start")]
        public async Task<IHttpActionResult> Trip_Start(int CustomerId)
        {
            var quote = abstractCustomerServices.Trip_Start(CustomerId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_Delete")]
        public async Task<IHttpActionResult> Customer_Delete(int Id, int DeletedBy)
        {
            var quote = abstractCustomerServices.Customer_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // Customer_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_Login")]
        public async Task<IHttpActionResult> Customer_Login(string Email, string PasswordHash , string DeviceToken)
        {
            var quote = abstractCustomerServices.Customer_Login(Email, PasswordHash, DeviceToken);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_Logout")]
        public async Task<IHttpActionResult> Customer_Logout(int Id)
        {
            var quote = abstractCustomerServices.Customer_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        // Customer_ChangePassword API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_ChangePassword")]
        public async Task<IHttpActionResult> Customer_ChangePassword(int Id,  string NewPassword, string ConfirmPassword, int Type,string MobileNumber = "",string OldPassword = "")
        {
            var quote = abstractCustomerServices.Customer_ChangePassword( Id, OldPassword,  NewPassword,  ConfirmPassword, MobileNumber,  Type);
            if (quote.Item != null && quote.Code == 200)
            {
                AbstractCustomerNotifications abstractCustomerNotifications = new CustomerNotifications();
                abstractCustomerNotifications.CustomerId = quote.Item.Id;
                abstractCustomerNotifications.Message = "Your Password Change Successfully";

                abstractCustomerNotificationsServices.CustomerNotifications_Upsert(abstractCustomerNotifications);
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }  
        
        //// Customer_Login API
        //[System.Web.Http.HttpPost]
        //[InheritedRoute("Customer_ChangePassword")]
        //public async Task<IHttpActionResult> Customer_ChangePassword(Customer customer)
        //{
        //    var quote = abstractCustomerServices.Customer_ChangePassword(customer);
        //    if (quote.Item != null && quote.Code == 200)
        //    {
        //        AbstractCustomerNotifications abstractCustomerNotifications = new CustomerNotifications();
        //        abstractCustomerNotifications.CustomerId = quote.Item.Id;
        //        abstractCustomerNotifications.Message = "Your Change Your Password Successfully";

        //        abstractCustomerNotificationsServices.CustomerNotifications_Upsert(abstractCustomerNotifications);
        //    }
        //    return this.Content((HttpStatusCode)quote.Code, quote);
        //}
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_AddressUpdate")]
        public async Task<IHttpActionResult> Customer_AddressUpdate(Customer customer)
        {
            var quote = abstractCustomerServices.Customer_AddressUpdate(customer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_Upsert")]
        public async Task<IHttpActionResult> Customer_Upsert(Customer customer)
        {
            var quote = abstractCustomerServices.Customer_Upsert(customer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_ProfilePictureUpdate")]
        public async Task<IHttpActionResult> Customer_ProfilePictureUpdate()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Customer Customer = new Customer();
            var httpRequest = HttpContext.Current.Request;
            Customer.CustomerId = Convert.ToInt32(httpRequest.Params["CustomerId"]);
            Customer.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CustomerProfilePicure/" + Customer.CustomerId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Customer.ProfilePicture = basePath + fileName;
            }

            var quote = abstractCustomerServices.Customer_ProfilePictureUpdate(Customer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_IdProofUpdate")]
        public async Task<IHttpActionResult> Customer_IdProofUpdate()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Customer Customer = new Customer();
            var httpRequest = HttpContext.Current.Request;
            Customer.CustomerId = Convert.ToInt32(httpRequest.Params["CustomerId"]);
            Customer.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CustomerIdProof/" + Customer.CustomerId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Customer.IdProof = basePath + fileName;
            }

            var quote = abstractCustomerServices.Customer_IdProofUpdate(Customer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_RcCardUpdate")]
        public async Task<IHttpActionResult> Customer_RcCardUpdate()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Customer Customer = new Customer();
            var httpRequest = HttpContext.Current.Request;
            Customer.CustomerId = Convert.ToInt32(httpRequest.Params["CustomerId"]);
            Customer.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CustomerRcCard/" + Customer.CustomerId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Customer.RcCard = basePath + fileName;
            }

            var quote = abstractCustomerServices.Customer_RcCardUpdate(Customer);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_All")]
        public async Task<IHttpActionResult> Customer_All(PageParam pageParam, string search = "", int IsActiveForFilter = 0, string Gender = "")
        {
            AbstractCustomer customer = new Customer();
            customer.IsActiveForFilter = IsActiveForFilter;
            customer.Gender = Gender;

            var quote = abstractCustomerServices.Customer_All(pageParam, search, customer);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
