﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterTripCancelReasonV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterTripCancelReasonServices abstractMasterTripCancelReasonServices;
        #endregion

        #region Cnstr
        public MasterTripCancelReasonV1Controller(AbstractMasterTripCancelReasonServices abstractMasterTripCancelReasonServices)
        {
            this.abstractMasterTripCancelReasonServices = abstractMasterTripCancelReasonServices;
        }
        #endregion


        //MasterTripCancelReason_Upsert Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterTripCancelReason_Upsert")]
        public async Task<IHttpActionResult> MasterTripCancelReason_Upsert(MasterTripCancelReason masterTripCancelReason)
        {
            var quote = abstractMasterTripCancelReasonServices.MasterTripCancelReason_Upsert(masterTripCancelReason);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
      
       
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterTripCancelReason_All")]
        public async Task<IHttpActionResult> MasterTripCancelReason_All(PageParam pageParam, string search = "")
        {
            var quote = abstractMasterTripCancelReasonServices.MasterTripCancelReason_All(pageParam, search );
            return this.Content((HttpStatusCode)200, quote);
        }

        
    }
}
