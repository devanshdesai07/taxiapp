﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PromoCodeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPromoCodeServices abstractPromoCodeServices;
        #endregion

        #region Cnstr
        public PromoCodeV1Controller(AbstractPromoCodeServices abstractPromoCodeServices)
        {
            this.abstractPromoCodeServices = abstractPromoCodeServices;
        }
        #endregion       
        //PromoCode_Delete Api
        // [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("PromoCode_Delete")]
        public async Task<IHttpActionResult> PromoCode_Delete(int Id)
        {
            var quote = abstractPromoCodeServices.PromoCode_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        //PromoCode_ById Api
        // [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("PromoCode_ById")]
        public async Task<IHttpActionResult> PromoCode_ById(int Id)
        {
            var quote = abstractPromoCodeServices.PromoCode_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //PromoCode_Upsert Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("PromoCode_Upsert")]
        public async Task<IHttpActionResult> PromoCode_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            PromoCode PromoCode = new PromoCode();
            var httpRequest = HttpContext.Current.Request;
            PromoCode.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            PromoCode.Name = Convert.ToString(httpRequest.Params["Name "]);
            PromoCode.Discount = Convert.ToInt32(httpRequest.Params["Discount "]);
            PromoCode.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);
            PromoCode.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);
            var quote = abstractPromoCodeServices.PromoCode_Upsert(PromoCode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //PromoCode_All Api
        //[System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [InheritedRoute("PromoCode_All")]
        public async Task<IHttpActionResult> PromoCode_All(PageParam pageParam)
        {

            var quote = abstractPromoCodeServices.PromoCode_All(pageParam);
            return this.Content((HttpStatusCode)200, quote);
        }

        
    }
}
