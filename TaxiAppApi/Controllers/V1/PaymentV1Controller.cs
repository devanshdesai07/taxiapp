﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractPaymentServices AbstractPaymentServices;
        #endregion

        #region Cnstr
        public PaymentV1Controller(AbstractPaymentServices abstractPaymentServices)
        {
            this.AbstractPaymentServices = abstractPaymentServices;
        }
        #endregion
  
        //Users_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_All")]
        public async Task<IHttpActionResult> Payment_All(PageParam pageParam, string search = "", int DriverId=0)
        {
            var quote = AbstractPaymentServices.Payment_All(pageParam, search, 0, DriverId, 0);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Payment_Upsert")]
        public async Task<IHttpActionResult> Payment_Upsert(Payment payment)
        {
            var quote = AbstractPaymentServices.Payment_Upsert(payment);
           
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
