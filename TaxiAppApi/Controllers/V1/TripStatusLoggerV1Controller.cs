﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TripStatusLoggerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTripStatusLoggerServices abstractTripStatusLoggerServices;
        #endregion

        #region Cnstr
        public TripStatusLoggerV1Controller(AbstractTripStatusLoggerServices abstractTripStatusLoggerServices)
        {
            this.abstractTripStatusLoggerServices = abstractTripStatusLoggerServices;
        }
        #endregion

        
        // TripStatusLogger_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_UpsertStatus")]
        public async Task<IHttpActionResult> Trip_UpsertStatus(TripStatusLogger tripStatusLogger)
        {
            var quote = abstractTripStatusLoggerServices.Trip_UpsertStatus(tripStatusLogger);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
