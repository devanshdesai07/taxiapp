﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TripV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTripServices abstractTripServices;
        private readonly AbstractDriverNotificationsServices abstractDriverNotificationsServices;
        #endregion

        #region Cnstr
        public TripV1Controller(AbstractTripServices abstractTripServices, AbstractDriverNotificationsServices abstractDriverNotificationsServices)
        {
            this.abstractTripServices = abstractTripServices;
            this.abstractDriverNotificationsServices = abstractDriverNotificationsServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("TripRating_Upsert")]
        public async Task<IHttpActionResult> TripRating_Upsert(int id, int Rating)
        {
            var quote = abstractTripServices.TripRating_Upsert(id, Rating);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Trip_ById Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_ById")]
        public async Task<IHttpActionResult> Trip_ById(int Id)
        {
            var quote = abstractTripServices.Trip_ById(Id);
            
            //var totaltime = quote.Item.TotalTime;
            //if (totaltime.Contains("days") && !totaltime.Contains("hours") && !totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    quote.Item.TotalTime = Convert.ToString(((days * 24))) ;
            //}
            //else  if (totaltime.Contains("days") && totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    int hours = Convert.ToInt32(abc[2]);
            //    int minutes = Convert.ToInt32(abc[4]);
            //    quote.Item.TotalTime = Convert.ToString(((days * 24) + hours)) + '.' + minutes;
            //}
            
            //else if (!totaltime.Contains("days") && totaltime.Contains("hours") && !totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int newvar = Convert.ToInt32(abc[0]);
            //    quote.Item.TotalTime = Convert.ToString(newvar);
            //}
            //else if (!totaltime.Contains("days") && !totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
                
            //    var abc = totaltime.Split(' ');
            //    int minutes = Convert.ToInt32(abc[0]);
            //     var aaa = '.' + abc[0];
            //    quote.Item.TotalTime = 0 + aaa;
            //}
            //else if (totaltime.Contains("days") && totaltime.Contains("hours") && !totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    int hours = Convert.ToInt32(abc[2]);
            //    quote.Item.TotalTime = Convert.ToString(((days * 24) + hours));
            //}
            //else if (totaltime.Contains("days") && !totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    int minutes = Convert.ToInt32(abc[2]);
            //    quote.Item.TotalTime = Convert.ToString((days * 24)) + '.' + minutes;
            //}
            //else if (!totaltime.Contains("days") && totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int hours = Convert.ToInt32(abc[0]);
            //    int minutes = Convert.ToInt32(abc[2]);
            //    quote.Item.TotalTime = Convert.ToString((hours)) + '.' + minutes;
            //}
           
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //PromoCode_Apply Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("PromoCode_Apply")]
        public async Task<IHttpActionResult> PromoCode_Apply(int TripId, string Promocode)
        {
            var quote = abstractTripServices.PromoCode_Apply(TripId, Promocode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //PromoCode_Remove Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("PromoCode_Remove")]
        public async Task<IHttpActionResult> PromoCode_Remove(int TripId)
        {
            var quote = abstractTripServices.PromoCode_Remove(TripId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_Delete")]
        public async Task<IHttpActionResult> Trip_Delete(int Id, int DeletedBy)
        {
            var quote = abstractTripServices.Trip_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TripTempAssignDriver_Upsert")]
        public async Task<IHttpActionResult> TripTempAssignDriver_Upsert(int Id, int TripId)
        {
            var quote = abstractTripServices.TripTempAssignDriver_Upsert(Id, TripId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpGet]
        [InheritedRoute("Users_SendSMS")]
        public async Task<IHttpActionResult> Users_SendSMS(string mobilenumber)
        {
            string MobileNo = mobilenumber.ToString();

            int otp = ((int)SendSMSAPI(MobileNo));


            return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);
        }


        private int SendSMSAPI(string MobileNo)
        {
            int otp = Convert.ToInt32(new Random().Next(0, 9999).ToString("D4"));

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConvertTo.String(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", Convert.ToString(MobileNo)).Replace("#otp#", Convert.ToString(otp)));
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(response))
                    {
                        Root root = JsonConvert.DeserializeObject<Root>(response);
                    }

                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            return otp;

        }

        //Users_SendOTP API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_SendOtp")]
        public async Task<IHttpActionResult> Trip_SendOtp(string MobileNo)
        {
            var get_otp = SendSMSAPI(MobileNo);
            int Otp = ((int)get_otp);
            var quote = abstractTripServices.Trip_SendOtp(MobileNo, Otp);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Trip_VerifyOtp API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_VerifyOtp")]
        public async Task<IHttpActionResult> Trip_VerifyOtp(string MobileNo, int Otp)
        {
            var quote = abstractTripServices.Trip_VerifyOtp(MobileNo, Otp);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Trip_RejectByDriver API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_RejectByDriver")]
        public async Task<IHttpActionResult> Trip_RejectByDriver(int DriverId, int TripId)
        {
            var quote = abstractTripServices.Trip_RejectByDriver(DriverId, TripId);
            
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Trip_AcceptByDriver API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_AcceptByDriver")]
        public async Task<IHttpActionResult> Trip_AcceptByDriver(int DriverId, int TripId,string Polyline1)
        {
            var quote = abstractTripServices.Trip_AcceptByDriver(DriverId, TripId, Polyline1);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Trip_Begin API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_Begin")]
        public async Task<IHttpActionResult> Trip_Begin(int TripId, int DriverId, int Otp, string Polyline2, DateTime BeginTripTime, DateTime BeginTripDate)
        {
            var quote = abstractTripServices.Trip_Begin(TripId, DriverId,Otp,Polyline2,BeginTripTime,BeginTripDate);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Trip_End API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_End")]
        public async Task<IHttpActionResult> Trip_End(int DriverId, int TripId, DateTime EndTripTime, DateTime EndTripDate, string EndTripActualHour, string EndTripHour )
        {
            var quote = abstractTripServices.Trip_End(DriverId, TripId,EndTripTime,EndTripDate,EndTripActualHour,EndTripHour);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        //Trip_StartbyDriver API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_StartbyDriver")]
        public async Task<IHttpActionResult> Trip_StartbyDriver(int DriverId, int TripId, string DriverLocation="", string CustomerPickUpLocation="")
        {
            var quote = abstractTripServices.Trip_StartbyDriver(DriverId, TripId, DriverLocation, CustomerPickUpLocation);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_Upsert")]
        public async Task<IHttpActionResult> Trip_Upsert(Trip trip)
        {
            var quote = abstractTripServices.Trip_Upsert(trip);
            //var totaltime = quote.Item.TotalTime;
            //if (totaltime.Contains("days") && !totaltime.Contains("hours") && !totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    quote.Item.TotalTime = Convert.ToString(((days * 24)));
            //}
            //else if (totaltime.Contains("days") && totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    int hours = Convert.ToInt32(abc[2]);
            //    int minutes = Convert.ToInt32(abc[4]);
            //    quote.Item.TotalTime = Convert.ToString(((days * 24) + hours)) + '.' + minutes;
            //}

            //else if (!totaltime.Contains("days") && totaltime.Contains("hours") && !totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int newvar = Convert.ToInt32(abc[0]);
            //    quote.Item.TotalTime = Convert.ToString(newvar);
            //}
            //else if (!totaltime.Contains("days") && !totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{

            //    var abc = totaltime.Split(' ');
            //    int minutes = Convert.ToInt32(abc[0]);
            //    var aaa = '.' + abc[0];
            //    quote.Item.TotalTime = 0 + aaa;
            //}
            //else if (totaltime.Contains("days") && totaltime.Contains("hours") && !totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    int hours = Convert.ToInt32(abc[2]);
            //    quote.Item.TotalTime = Convert.ToString(((days * 24) + hours));
            //}
            //else if (totaltime.Contains("days") && !totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int days = Convert.ToInt32(abc[0]);
            //    int minutes = Convert.ToInt32(abc[2]);
            //    quote.Item.TotalTime = Convert.ToString((days * 24)) + '.' + minutes;
            //}
            //else if (!totaltime.Contains("days") && totaltime.Contains("hours") && totaltime.Contains("mins"))
            //{
            //    var abc = totaltime.Split(' ');
            //    int hours = Convert.ToInt32(abc[0]);
            //    int minutes = Convert.ToInt32(abc[2]);
            //    quote.Item.TotalTime = Convert.ToString((hours)) + '.' + minutes;
            //}
            //{
            //    AbstractDriverNotifications abstractDriverNotifications = new DriverNotifications();
            //    abstractDriverNotifications.DriverId = quote.Item.Id;
            //    abstractDriverNotifications.TotalTimeInHours = quote.Item.TotalTime;

            //    abstractDriverNotificationsServices.DriverNotifications_Upsert(abstractDriverNotifications);
            //}
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_Assigned")]
        public async Task<IHttpActionResult> Driver_Assigned(Trip trip)
        {
            var quote = abstractTripServices.Driver_Assigned(trip);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_CancelByCustomer")]
        public async Task<IHttpActionResult> Trip_CancelByCustomer(int TripId, int MasterTripCancelReasonId)
        {
            var quote = abstractTripServices.Trip_CancelByCustomer(TripId,MasterTripCancelReasonId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        //Trip_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Trip_All")]
        public async Task<IHttpActionResult> Trip_All(PageParam pageParam, string search = "", int CustomerId = 0, int AssignedId = 0,string TripStatusId = "")
        {
            AbstractTrip Trip = new Trip();
            Trip.CustomerId = CustomerId;
            Trip.AssignedId = AssignedId;
          // Trip.TripStatusId = TripStatusId;
            //Trip.DriverId = DriverId;
            //Trip.TripDateFrom = TripDateFrom;
            //Trip.TripDateTo = TripDateTo;
            //Trip.TripTimeFrom = TripTimeFrom;
            //Trip.TripTimeTo = TripTimeTo;

            var quote = abstractTripServices.Trip_All(pageParam, search, Trip, TripStatusId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Driver_TripAllDetail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_TripAllDetail")]
        public async Task<IHttpActionResult> Driver_TripAllDetail(PageParam pageParam, int DriverId, int TripStatusId,string search="")
        {
   
            var quote = abstractTripServices.Driver_TripAllDetail(pageParam, search, DriverId,TripStatusId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Customer_TripAllDetail Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customer_TripAllDetail")]
        public async Task<IHttpActionResult> Customer_TripAllDetail(PageParam pageParam, int CustomerId, int TripStatusId, string search="")
        {


            var quote = abstractTripServices.Customer_TripAllDetail(pageParam, search, CustomerId,TripStatusId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //Trip_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("TripList_ApprovalPending")]
        public async Task<IHttpActionResult> TripList_ApprovalPending(PageParam pageParam, int DriverId = 0)
        {
            AbstractTrip Trip = new Trip();
            Trip.DriverId = DriverId;
            var quote = abstractTripServices.TripList_ApprovalPending(pageParam, Trip);
            return this.Content((HttpStatusCode)200, quote);
        }

        public class Root
        {
            public string response { get; set; }
            public string responseCode { get; set; }
        }

    }
}
