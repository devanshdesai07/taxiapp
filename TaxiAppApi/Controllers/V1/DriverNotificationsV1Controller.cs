﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DriverNotificationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDriverNotificationsServices abstractDriverNotificationsServices;
        #endregion

        #region Cnstr
        public DriverNotificationsV1Controller(AbstractDriverNotificationsServices abstractDriverNotificationsServices)
        {
            this.abstractDriverNotificationsServices = abstractDriverNotificationsServices;
        }
        #endregion

        //DriverNotifications_ActInact Api
        [System.Web.Http.HttpPost]
      
        [InheritedRoute("DriverNotifications_ReadUnRead")]
        public async Task<IHttpActionResult> DriverNotifications_ReadUnRead(int Id)
        {
            var quote = abstractDriverNotificationsServices.DriverNotifications_ReadUnRead(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //DriverNotifications_ById Api
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverNotifications_ById")]
        public async Task<IHttpActionResult> DriverNotifications_ById(int Id)
        {
            var quote = abstractDriverNotificationsServices.DriverNotifications_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
       
        //DriverNotifications_Upsert Api
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverNotifications_Upsert")]
        public async Task<IHttpActionResult> DriverNotifications_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            DriverNotifications DriverNotifications = new DriverNotifications();
            var httpRequest = HttpContext.Current.Request;
            DriverNotifications.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            DriverNotifications.DriverId = Convert.ToInt32(httpRequest.Params["DriverId"]);
            DriverNotifications.Message = Convert.ToString(httpRequest.Params["Message"]);
            DriverNotifications.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);

            var quote = abstractDriverNotificationsServices.DriverNotifications_Upsert(DriverNotifications);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
     
        //DriverNotifications_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverNotifications_All")]
        public async Task<IHttpActionResult> DriverNotifications_All(PageParam pageParam, string search = "",int driverId = 0)
        {
            var quote = abstractDriverNotificationsServices.DriverNotifications_All(pageParam, search, driverId);
            return this.Content((HttpStatusCode)200, quote);
        }

        //DriverNotifications_Delete Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverNotifications_Delete")]
        public async Task<IHttpActionResult> DriverNotifications_Delete(int Id)
        {
            var quote = abstractDriverNotificationsServices.DriverNotifications_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


    }
}
