﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MasterServiceBaseV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterServiceBaseServices AbstractMasterServiceBaseServices;
        #endregion

        #region Cnstr
        public MasterServiceBaseV1Controller(AbstractMasterServiceBaseServices abstractMasterServiceBaseServices)
        {
            this.AbstractMasterServiceBaseServices = abstractMasterServiceBaseServices;
        }
        #endregion
  
        //Users_All Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceBase_All")]
        public async Task<IHttpActionResult> MasterServiceBase_All(PageParam pageParam, string search = "")
        {
            var quote = AbstractMasterServiceBaseServices.MasterServiceBase_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
