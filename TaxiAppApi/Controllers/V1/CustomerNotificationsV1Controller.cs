﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerNotificationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerNotificationsServices abstractCustomerNotificationsServices;
        #endregion

        #region Cnstr
        public CustomerNotificationsV1Controller(AbstractCustomerNotificationsServices abstractCustomerNotificationsServices)
        {
            this.abstractCustomerNotificationsServices = abstractCustomerNotificationsServices;
        }
        #endregion

        //CustomerNotifications_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerNotifications_ById")]
        public async Task<IHttpActionResult> CustomerNotifications_ById(int Id)
        {
            var quote = abstractCustomerNotificationsServices.CustomerNotifications_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //CustomerNotifications_ById Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerNotifications_Delete")]
        public async Task<IHttpActionResult> CustomerNotifications_Delete(int CustomerId)
        {
            var quote = abstractCustomerNotificationsServices.CustomerNotifications_Delete(CustomerId);
            return this.Content((HttpStatusCode)200, quote);
        }
        //DriverNotifications_Upsert Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerNotifications_Upsert")]
        public async Task<IHttpActionResult> CustomerNotifications_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            CustomerNotifications CustomerNotifications = new CustomerNotifications();
            var httpRequest = HttpContext.Current.Request;
            CustomerNotifications.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            CustomerNotifications.CustomerId = Convert.ToInt32(httpRequest.Params["CustomerId"]);
            CustomerNotifications.Message = Convert.ToString(httpRequest.Params["Message"]);
            CustomerNotifications.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);

            var quote = abstractCustomerNotificationsServices.CustomerNotifications_Upsert(CustomerNotifications);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //CustomerNotifications_All Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerNotifications_All")]
        public async Task<IHttpActionResult> CustomerNotifications_All(PageParam pageParam, string search = "",int CustomerId = 0)
        {
            CustomerNotifications CustomerNotifications = new CustomerNotifications();
            CustomerNotifications.CustomerId = CustomerId;
            var quote = abstractCustomerNotificationsServices.CustomerNotifications_All(pageParam, search, CustomerNotifications);
            return this.Content((HttpStatusCode)200, quote);
        }

        //CustomerNotifications_ActInact Api
        [System.Web.Http.HttpPost]
       
        [InheritedRoute("CustomerNotifications_ReadUnRead")]
        public async Task<IHttpActionResult> CustomerNotifications_ReadUnRead(int Id)
        {
            var quote = abstractCustomerNotificationsServices.CustomerNotifications_ReadUnRead(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}
