﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using Newtonsoft.Json;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DriverV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDriverServices abstractDriverServices;
        private readonly AbstractTripServices abstractTripServices;
        private readonly AbstractDriverNotificationsServices abstractDriverNotificationsServices;
        #endregion

        #region Cnstr
        public DriverV1Controller(
               AbstractDriverServices abstractDriverServices,
               AbstractTripServices abstractTripServices,
               AbstractDriverNotificationsServices abstractDriverNotificationsServices)
        {
            this.abstractDriverServices = abstractDriverServices;
            this.abstractTripServices = abstractTripServices;
            this.abstractDriverNotificationsServices = abstractDriverNotificationsServices;
        }
        #endregion



        // Driver_LatLon Api is a Auto Assign

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_LatLon")]
        public async Task<IHttpActionResult> Driver_LatLon(int id)
        {
            var quote = abstractDriverServices.Driver_LatLon(id);
            var CusId = abstractTripServices.Trip_ById(id);
            var km = quote.Item.DISTANCE;
            int Did = CusId.Item.CustomerId;
            Driver dr = new Driver();
            DriverNotifications Driver = new DriverNotifications();
            MsgResJsFormate msgResJs = new MsgResJsFormate();
            dr.Id = quote.Item.Id;

            Driver.DriverId = quote.Item.Id;
            msgResJs.DriverId = Driver.DriverId;

            Driver.CustomerId = CusId.Item.CustomerId;
            msgResJs.CustomerId = Driver.CustomerId;

            Driver.TripId = id;
            msgResJs.TripId = Driver.TripId;

            Driver.Message = "Are You Ready For Ride";
            msgResJs.Message = Driver.Message;
            msgResJs.Type = "Request";

            var myDesClass = JsonConvert.SerializeObject(msgResJs);
            dr.MsgJsonRes = myDesClass;

            var MsgUpdate = abstractDriverNotificationsServices.DriverNotifications_Upsert(Driver);
            var MsgSend = abstractDriverServices.Driver_MsgRec(dr);

            return this.Content((HttpStatusCode)quote.Code, MsgSend.Message);
        }
        #region temshort

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_ById")]
        public async Task<IHttpActionResult> Driver_ById(int Id)
        {
            var quote = abstractDriverServices.Driver_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_ProfilePictureUpdate")]
        public async Task<IHttpActionResult> Driver_ProfilePictureUpdate()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Driver Driver = new Driver();
            var httpRequest = HttpContext.Current.Request;
            Driver.DriverId = Convert.ToInt32(httpRequest.Params["DriverId"]);
            Driver.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "DriverProfilePicure/" + Driver.DriverId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Driver.ProfilePicture = basePath + fileName;
            }

            var quote = abstractDriverServices.Driver_ProfilePictureUpdate(Driver);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_IdProofUpdate")]
        public async Task<IHttpActionResult> Driver_IdProofUpdate()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Driver Driver = new Driver();
            var httpRequest = HttpContext.Current.Request;
            Driver.DriverId = Convert.ToInt32(httpRequest.Params["DriverId"]);
            Driver.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "DriverIdProof/" + Driver.DriverId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Driver.IdProof = basePath + fileName;
            }

            var quote = abstractDriverServices.Driver_IdProofUpdate(Driver);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_DrivingLicenceUpdate")]
        public async Task<IHttpActionResult> Driver_DrivingLicenceUpdate()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Driver Driver = new Driver();
            var httpRequest = HttpContext.Current.Request;
            Driver.DriverId = Convert.ToInt32(httpRequest.Params["DriverId"]);
            Driver.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "DriverLicence/" + Driver.DriverId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                Driver.DrivingLicence = basePath + fileName;
            }

            var quote = abstractDriverServices.Driver_DrivingLicenceUpdate(Driver);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Driver_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_Login")]
        public async Task<IHttpActionResult> Driver_Login(string Email, string PasswordHash, string DeviceToken)
        {
            var quote = abstractDriverServices.Driver_Login(Email, PasswordHash, DeviceToken);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Driver_Online API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_IsOnline")]
        public async Task<IHttpActionResult> Driver_IsOnline(int id, string Lat, string Long)
        {
            var quote = abstractDriverServices.Driver_IsOnline(id, Lat, Long);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Driver_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_AddressUpdate")]
        public async Task<IHttpActionResult> Driver_AddressUpdate(Driver driver)
        {
            var quote = abstractDriverServices.Driver_AddressUpdate(driver);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // Driver_Login API
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_ChangePassword")]
        public async Task<IHttpActionResult> Driver_ChangePassword(int Id,  string NewPassword, string ConfirmPassword, int Type, string MobileNumber = "", string OldPassword= "")
        {
            var quote = abstractDriverServices.Driver_ChangePassword(Id,OldPassword,NewPassword,ConfirmPassword,MobileNumber,Type);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Driver_Upsert Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_Upsert")]
        public async Task<IHttpActionResult> Driver_Upsert()
        {
            var claimsIdentity = (ClaimsIdentity)this.RequestContext.Principal.Identity;

            Driver Driver = new Driver();
            var httpRequest = HttpContext.Current.Request;
            Driver.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            Driver.FirstName = Convert.ToString(httpRequest.Params["FirstName"]);
            Driver.LastName = Convert.ToString(httpRequest.Params["LastName"]);
            Driver.MobileNo = Convert.ToString(httpRequest.Params["MobileNo"]);
            Driver.Gender = Convert.ToString(httpRequest.Params["Gender"]);
            Driver.Email = Convert.ToString(httpRequest.Params["Email"]);
            Driver.Password = Convert.ToString(httpRequest.Params["Password"]);
            Driver.CPassword = Convert.ToString(httpRequest.Params["CPassword"]);
            Driver.DOB = Convert.ToString(httpRequest.Params["DOB"]);
            Driver.LicenceNo = Convert.ToString(httpRequest.Params["LicenceNo"]);
            Driver.PucNo = Convert.ToString(httpRequest.Params["PucNo"]);
            Driver.LICNo = Convert.ToString(httpRequest.Params["LICNo"]);
            Driver.VehicleNo = Convert.ToString(httpRequest.Params["VehicleNo"]);
            Driver.ChasisNo = Convert.ToString(httpRequest.Params["ChasisNo"]);
            Driver.DriverRatings = Convert.ToDecimal(httpRequest.Params["DriverRatings"]);
            Driver.BloodGroup = Convert.ToString(httpRequest.Params["BloodGroup"]);
            Driver.CountryId = Convert.ToInt32(httpRequest.Params["CountryId"]);
            Driver.StateId = Convert.ToInt32(httpRequest.Params["StateId"]);
            Driver.CityId = Convert.ToInt32(httpRequest.Params["CityId"]);
            Driver.PinCode = Convert.ToInt32(httpRequest.Params["PinCode"]);
            Driver.Address = Convert.ToString(httpRequest.Params["Address"]);
            Driver.CreatedBy = Convert.ToInt32(httpRequest.Params["CreatedBy"]);
            Driver.UpdatedBy = Convert.ToInt32(httpRequest.Params["UpdatedBy"]);

            var quote = abstractDriverServices.Driver_Upsert(Driver);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Delete Api

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_Delete")]
        public async Task<IHttpActionResult> Driver_Delete(int Id, int DeletedBy)
        {
            var quote = abstractDriverServices.Driver_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_Logout")]
        public async Task<IHttpActionResult> Driver_Logout(int Id)
        {
            var quote = abstractDriverServices.Driver_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        //Driver_All Api


        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_All")]
        public async Task<IHttpActionResult> Driver_All(PageParam pageParam, string search = "", bool IsActive = true, string Gender = "")
        {
            AbstractDriver Driver = new Driver();
            Driver.IsActive = IsActive;
            Driver.Gender = Gender;
            var quote = abstractDriverServices.Driver_All(pageParam, search, Driver);
            return this.Content((HttpStatusCode)200, quote);
        }



        //Driver_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_ActInAct")]
        public async Task<IHttpActionResult> Driver_ActInAct(int Id)
        {
            var quote = abstractDriverServices.Driver_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Driver_IdProofApproved Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_IdProofApproved")]
        public async Task<IHttpActionResult> Driver_IdProofApproved(int Id)
        {
            var quote = abstractDriverServices.Driver_IdProofApproved(Id);
            if (quote.Item != null && quote.Code == 200)
            {
                AbstractDriverNotifications abstractDriverNotifications = new DriverNotifications();
                abstractDriverNotifications.DriverId = quote.Item.Id;
                abstractDriverNotifications.Message = "Your Id Proof Approved Successfully";

                abstractDriverNotificationsServices.DriverNotifications_Upsert(abstractDriverNotifications);
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //Driver_DriveingLicenceApproved Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_DriveingLicenceApproved")]
        public async Task<IHttpActionResult> Driver_DriveingLicenceApproved(int Id)
        {
            var quote = abstractDriverServices.Driver_DriveingLicenceApproved(Id);
            if (quote.Item != null && quote.Code == 200)
            {
                AbstractDriverNotifications abstractDriverNotifications = new DriverNotifications();
                abstractDriverNotifications.DriverId = quote.Item.Id;
                abstractDriverNotifications.Message = "Your Driving Licence Approved Successfully";

                abstractDriverNotificationsServices.DriverNotifications_Upsert(abstractDriverNotifications);
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        #endregion

        public class MsgResJsFormate
        {
            public int DriverId { get; set; }
            public int TripId { get; set; }
            public int CustomerId { get; set; }
            public string Message { get; set; }
            public string Type { get; set; }
        }

    }
}
