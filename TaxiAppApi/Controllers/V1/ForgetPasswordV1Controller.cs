﻿using TaxiApp.APICommon;
using TaxiAppApi.Models;
using TaxiApp.Common;
using TaxiAppApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;
using TaxiApp.Entities.V1;
using System.IO;
using System.Security.Claims;
using System.Web.Http.Cors;
using SendGrid.Helpers.Mail;
using SendGrid;
using System.Configuration;
using SendGrid.Helpers.Mail.Model;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace TaxiAppApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ForgetPasswordV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerServices abstractCustomerServices;
        private readonly AbstractDriverServices abstractDriverServices;
        #endregion

        #region Cnstr
        public ForgetPasswordV1Controller(AbstractCustomerServices abstractCustomerServices, AbstractDriverServices abstractDriverServices)
        {
            this.abstractCustomerServices = abstractCustomerServices;
            this.abstractDriverServices = abstractDriverServices;
        }
        #endregion
        // ForgetPassword_SendMail API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ForgetPassword_SendMail")]
        public async Task<IHttpActionResult> ForgetPassword_SendMail(string Email,bool isCustomer)
        {
            try
            {
                
                if (isCustomer)
                {
                    var quote = abstractCustomerServices.Customer_ByEmail(Email);
                    await SendEmailUsingSendGridMail(quote.Item.Email,quote.Item.FullName,"Test Subject",quote.Item.FullName,quote.Item.Password);
                }
                else
                {
                    var quote = abstractDriverServices.Driver_ByEmail(Email);
                    await SendEmailUsingSendGridMail(quote.Item.Email, quote.Item.FullName, "Test Subject", quote.Item.FullName, quote.Item.Password);
                }
                
                return this.Content((HttpStatusCode)200, "Ok");
            }
            catch (Exception ex)
            {
                return this.Content((HttpStatusCode)400, ex.Message.ToString());
            }
            
        }
        //private async Task<bool> SendEmailUsingSendGridMail(string emailTo, string reciepantName, string Subject = "", string Name = "", string OldPassword = "")
        //{
        //    var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

        //    //var sendGridMessage = new SendGridMessage();
        //    //sendGridMessage.SetFrom(ConfigurationManager.AppSettings["SendGridFromEmail"], "Rushkar");
        //    //sendGridMessage.AddTo(emailTo); // user email
        //    //sendGridMessage.SetTemplateId("d-aa03e3c983424c288fd97779bd11cb9a");
        //    //sendGridMessage.SetTemplateData(new { Subject = Subject, Name = Name,OldPassowrd = OldPassword });
        //    string PlainTextContent = "Hi " + Name + ", \nYour password is -> " + OldPassword + " \nThank you.";
        //    //sendGridMessage.Subject = Subject;
        //    string HtmlContent = "Hi " + Name + "<br/>Your password is -> " + OldPassword + "<br/>Thank you.";
        //    var msg = MailHelper.CreateSingleEmail(new EmailAddress { Email = ConfigurationManager.AppSettings["SendGridFromEmail"], Name = "Rushkar" }, new EmailAddress { Email = emailTo, Name = Name }, Subject, PlainTextContent, HtmlContent);
        //    var response = sendGridClient.SendEmailAsync(msg).Result;
        //    if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}

        private async Task<bool> SendEmailUsingSendGridMail(string emailTo, string reciepantName, string Subject = "", string Name = "", string OldPassword = "")
        {
            var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"]);

            string body = "";
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplateForForgetpw.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{{Name}}", Name);
            body = body.Replace("{{OldPassword}}", OldPassword);
            body = body.Replace("{{Subject}}", Subject);

            string PlainTextContent = HtmlToPlainText(body);

            var msg = MailHelper.CreateSingleEmail(new EmailAddress { Email = ConfigurationManager.AppSettings["SendGridFromEmail"], Name = "Rushkar" }, new EmailAddress { Email = emailTo, Name = Name }, Subject, PlainTextContent, body);
            var response = sendGridClient.SendEmailAsync(msg).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
    }
}
