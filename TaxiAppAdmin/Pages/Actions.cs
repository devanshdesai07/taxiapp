﻿namespace TaxiAppAdmin.Pages
{
    public class Actions
    {
        
        public const string DriverSendNotifications_Upsert = "DriverSendNotifications_Upsert";
        public const string DriverNotifications_Upsert = "DriverNotifications_Upsert";
        public const string CustomerNotifications_Upsert = "CustomerNotifications_Upsert";
        public const string Payment_Upsert = "Payment_Upsert";
        public const string Signin = "Signin";
        public const string GetPaymentById = "GetPaymentById"; 
        public const string Salons = "Salons";
        public const string Signout = "Signout";
        public const string Index = "Index";
        public const string Delete = "Delete";
        public const string ViewAllData = "ViewAllData";
        public const string ViewAllDataSalons = "ViewAllDataSalons";
        public const string ViewAllDataCustomer = "ViewAllDataCustomer";
        public const string ActiveInActive = "ActiveInActive";
        public const string UserGallery = "UserGallery";
        public const string ChangeStatusSalon = "ChangeStatusSalon";
        public const string ManageCustomers = "ManageCustomers";
        public const string ManageEmployeeLeaves = "ManageEmployeeLeaves";
        public const string ViewAllDataEmployees = "ViewAllDataEmployees";
        public const string ViewAllDataEmployeeLeaves = "ViewAllDataEmployeeLeaves";
        public const string ManageEmployeeWorksheet = "ManageEmployeeWorksheet";
        public const string ViewAllDataEmployeeWorkSheet = "ViewAllDataEmployeeWorkSheet";
        public const string ManageEmployees = "ManageEmployees";
        public const string ManageSalonServices = "ManageSalonServices";
        public const string ViewAllDataSalonServices = "ViewAllDataSalonServices";
        public const string ChangePassword = "ChangePassword";
        public const string ViewAllDataAppointments = "ViewAllDataAppointments";
        public const string ManageAppointments = "ManageAppointments";
        public const string BindServicesDropdown = "BindServicesDropdown";
        public const string ViewDataCustomerDetails = "ViewDataCustomerDetails";
        public const string ViewAllDataEmployeeLeavecount = "ViewAllDataEmployeeLeavecount";
        public const string GetAppointmentDetaislById = "GetAppointmentDetaislById";
        public const string ManageEmployeeAndCharges = "ManageEmployeeAndCharges";
        public const string ViewAllDataEmployeeAndCharges = "ViewAllDataEmployeeAndCharges";
        public const string ViewAllDataMasterProductType = "ViewAllDataMasterProductType";
        public const string MasterProductType = "MasterProductType";
        public const string ActiveInActiveProductType = "ActiveInActiveProductType";
        public const string DeleteProductType = "DeleteProductType";
        public const string ProductTypeUpsert = "ProductTypeUpsert";
        public const string GetDataProductTypeById = "GetDataProductTypeById";

        public const string GetDataProductBrandById = "GetDataProductBrandById";
        public const string ProductBrandUpsert = "ProductBrandUpsert";
        public const string DeleteProductBrand = "DeleteProductBrand";
        public const string ViewAllDataMasterProductBrand = "ViewAllDataMasterProductBrand";
        public const string ActiveInActiveProductBrand = "ActiveInActiveProductBrand";
        public const string MasterProductBrand = "MasterProductBrand";

        public const string ViewAllDataMasterProductWeight = "ViewAllDataMasterProductWeight";
        public const string ActiveInActiveProductWeight = "ActiveInActiveProductWeight";
        public const string DeleteProductWeight = "DeleteProductWeight";
        public const string ProductWeightUpsert = "ProductWeightUpsert";
        public const string GetDataProductweightById = "GetDataProductweightById";
        public const string MasterProductWeight = "MasterProductWeight";

        public const string ViewAllDataMasterOrderStatus = "ViewAllDataMasterOrderStatus";
        public const string ActiveInActiveOrderStatus = "ActiveInActiveOrderStatus";
        public const string DeleteOrderStatus = "DeleteOrderStatus";
        public const string OrderStatusUpsert = "OrderStatusUpsert";
        public const string GetDataOrderstatusById = "GetDataOrderstatusById";
        public const string MasterOrderStatus = "MasterOrderStatus";
        public const string MasterProductCategory = "MasterProductCategory";
        public const string ViewAllDataMasterProductCategory = "ViewAllDataMasterProductCategory";
        public const string ActiveInActiveProductCategory = "ActiveInActiveProductCategory";
        public const string DeleteProductCategory = "DeleteProductCategory";
        public const string ProductCategoryUpsert = "ProductCategoryUpsert";
        public const string GetDataProductCategoryById = "GetDataProductCategoryById";

        public const string LogIn = "LogIn";
        public const string Logout = "Logout";
        public const string TripDetails = "TripDetails";
        public const string CustomerDetails = "CustomerDetails";
        public const string ViewAllDataTrips = "ViewAllDataTrips";
        public const string ChangeStatusTrip = "ChangeStatusTrip";
        public const string AssignedDriver = "AssignedDriver";
        public const string AddFaq = "AddFaq";
        public const string MyDetails = "MyDetails";
        public const string DriverDetails = "DriverDetails";
        public const string FaqUpsert = "FaqUpsert";
        public const string ViewAllUnAssignedTripData = "ViewAllUnAssignedTripData";
        public const string ViewAllAssignedTripData = "ViewAllAssignedTripData";
        public const string GetFaqById = "GetFaqById";
        public const string ViewAllCompletedTripData = "ViewAllCompletedTripData";
        public const string ViewAllConfirmedTripData = "ViewAllConfirmedTripData";
        public const string ViewAllRejectedTripData = "ViewAllRejectedTripData";
        public const string DeleteFaq = "DeleteFaq";
        public const string ApprovedUnApprovedIdProof = "ApprovedUnApprovedIdProof";
        public const string ApprovedUnApprovedDrivingLicence = "ApprovedUnApprovedDrivingLicence";
        public const string ApprovedUnApprovedRcCard = "ApprovedUnApprovedRcCard";
        public const string GetPricePackageById = "GetPricePackageById";
        public const string PricePackageUpsert = "PricePackageUpsert";
        public const string PromoCodeUpsert = "PromoCodeUpsert";
        public const string GetPromoCodeById = "GetPromoCodeById";
        public const string DeletePromoCode = "DeletePromoCode";
        public const string GetLatLongById = "GetLatLongById";

        public const string NotesAllData = "NotesAllData";
        public const string NotesUpsert = "NotesUpsert";
        public const string DeleteNotes = "DeleteNotes";
        public const string GetNotesById = "GetNotesById";
        public const string TripPageDetails = "TripPageDetails";
        public const string TripDetail = "TripDetail";

        public const string ViewAllNewTripData = "ViewAllNewTripData";
        public const string ViewAllOngoingTripsData = "ViewAllOngoingTripsData";
        public const string ViewAllAcceptedTripData = "ViewAllAcceptedTripData";
        
    }
}