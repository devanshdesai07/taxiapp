﻿using System.Collections.Generic;

namespace TaxiAppAdmin.Pages
{
    public class Controllers
    {
        public const string Account = "Account";
        public const string CustomerNotifications = "CustomerNotifications";
        public const string DriverNotifications = "DriverNotifications";
        public const string Home = "Home";
        public const string ChangePassword = "ChangePassword";
        public const string Customer = "Customer";
        public const string Driver = "Driver";
        public const string Faq = "Faq";
        public const string Trip = "Trip";
        public const string PackagePrice = "PackagePrice";
        public const string PromoCode = "PromoCode";
        public const string Authentication = "Authentication";
        public const string PackagePriceOtherRate = "PackagePriceOtherRate";
        public const string MasterNotes = "MasterNotes";
        public const string Payment = "Payment";
        public const string DriverSendNotifications = "DriverSendNotifications";
        

    }
}