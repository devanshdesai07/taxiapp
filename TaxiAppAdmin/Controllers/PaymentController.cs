﻿using DataTables.Mvc;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using TaxiApp.Services.Contract;
using TaxiAppAdmin.Infrastructure;
using TaxiAppAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TaxiAppAdmin.Controllers
{
    public class PaymentController : BaseController
    {
        private readonly AbstractPaymentServices abstractPaymentServices = null;


        public PaymentController(AbstractPaymentServices abstractPaymentServices)
        {
            this.abstractPaymentServices = abstractPaymentServices;

        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPaymentById(int Id = 0)
        {
            SuccessResult<AbstractPayment> successResult = abstractPaymentServices.Payment_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ViewAllData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel , int TripId = 0, int DriverId = 0, int CustomerId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractPayment Payment = new Payment();
              //  Payment.IsVisibleAll = IsVisibleAll;
                Payment.CustomerId = CustomerId;
                Payment.DriverId = DriverId;
                Payment.TripId = TripId;
               

                var response = abstractPaymentServices.Payment_All(pageParam, search, TripId, DriverId, CustomerId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpPost]
       // [ValidateInput(false)]
        public JsonResult PaymentUpsert(Payment payment)
        {
            
            //Payment model = new Payment();
            //model.Id = Id;
            //model.DriverId = DriverId;
            //model.TripId = TripId;
            //model.Amount = Amount;
            //model.CreatedBy = ProjectSession.AdminId;
            //model.UpdatedBy = ProjectSession.AdminId;

            var result = abstractPaymentServices.Payment_Upsert(payment);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}