﻿using DataTables.Mvc;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using TaxiApp.Services.Contract;
using TaxiAppAdmin.Infrastructure;
using TaxiAppAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TaxiAppAdmin.Controllers
{
    public class MasterNotesController : BaseController
    {
        private readonly AbstractMasterNotesServices abstractMasterNotesServices = null;

        public MasterNotesController(AbstractMasterNotesServices abstractMasterNotesServices)
        {
            this.abstractMasterNotesServices = abstractMasterNotesServices;
        }

        public ActionResult Index()
        {
            return View();
        }

            
        [HttpPost]
        public JsonResult DeleteNotes(int Id = 0)
        {
            int DeletedBy = ProjectSession.AdminId;
            abstractMasterNotesServices.MasterNotes_Delete(Id, DeletedBy);
            return Json("Note Deleted successfully", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetNotesById(int Id = 0)
        {
            SuccessResult<AbstractMasterNotes> successResult = abstractMasterNotesServices.MasterNotes_ById(Id);
            return Json(successResult,JsonRequestBehavior.AllowGet);
        }
        
        //Faq owner all data get
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult NotesAllData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ForFaq = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var response = abstractMasterNotesServices.MasterNotes_All(pageParam, search);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult NotesUpsert( int Id = 0, string Notes = "")
        {

            MasterNotes model = new MasterNotes();
            model.Id = Id;
            model.Notes = Notes;
            model.CreatedBy = ProjectSession.AdminId;
            model.UpdatedBy = ProjectSession.AdminId;

            var result = abstractMasterNotesServices.MasterNotes_Upsert(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}