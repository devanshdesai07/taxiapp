﻿using DataTables.Mvc;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using TaxiApp.Services.Contract;
using TaxiAppAdmin.Infrastructure;
using TaxiAppAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TaxiAppAdmin.Controllers
{
    public class CustomerNotificationsController : BaseController
    {
        public readonly AbstractCustomerNotificationsServices abstractCustomerNotificationsServices;

        public CustomerNotificationsController(AbstractCustomerNotificationsServices abstractCustomerNotificationsServices)
        {
            this.abstractCustomerNotificationsServices = abstractCustomerNotificationsServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            ViewBag.CustomersAll = CustomersDrp();
            return View();
        }
        [HttpPost]

        public JsonResult CustomerNotifications_ById(int Id = 0)
        {
            SuccessResult<AbstractCustomerNotifications> successResult = abstractCustomerNotificationsServices.CustomerNotifications_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> CustomersDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractCustomerNotificationsServices.CustomerNotifications_Dropdown(pageParam);

                foreach (var master in models.Values)
                {
                    if (master.FirstName != null && master.MobileNo != "")
                    {
                        items.Add(new SelectListItem() { Text = master.FirstName.ToString() + " | " + master.MobileNo.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }

                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        //UserNotification owner all data get
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CustomerNotifications_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AbstractCustomerNotifications abstractCustomerNotifications)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractCustomerNotificationsServices.CustomerNotifications_All(pageParam, search, abstractCustomerNotifications);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CustomerNotifications_Upsert(CustomerNotifications customerNotifications)
        {

            customerNotifications.CreatedBy = ProjectSession.AdminId;

            var result = abstractCustomerNotificationsServices.CustomerNotifications_Upsert(customerNotifications);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}