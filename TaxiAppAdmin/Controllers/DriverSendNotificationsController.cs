﻿using DataTables.Mvc;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using TaxiApp.Services.Contract;
using TaxiAppAdmin.Infrastructure;
using TaxiAppAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TaxiAppAdmin.Controllers
{
    public class DriverSendNotificationsController : BaseController
    {
        public readonly AbstractDriverNotificationsServices abstractDriverNotificationsServices;

        public DriverSendNotificationsController(AbstractDriverNotificationsServices abstractDriverNotificationsServices)
        {
            this.abstractDriverNotificationsServices = abstractDriverNotificationsServices;
        }

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            ViewBag.DriversAll = DriversDrp();
            return View();
        }
        [HttpPost]

        public JsonResult DriverNotifications_ById(int Id = 0)
        {
            SuccessResult<AbstractDriverNotifications> successResult = abstractDriverNotificationsServices.DriverNotifications_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> DriversDrp()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                var models = abstractDriverNotificationsServices.DriverSendNotifications_Dropdown(pageParam);

                foreach (var master in models.Values)
                {
                    if (master.FirstName != null && master.MobileNo != "")
                    {
                        items.Add(new SelectListItem() { Text = master.FirstName.ToString() + " | " + master.MobileNo.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }

                return items;
            }
            catch (Exception)
            {
                return items;
            }
        }

        //UserNotification owner all data get
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DriverNotifications_All([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int DriverId = 0)
        {
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                var response = abstractDriverNotificationsServices.DriverNotifications_All(pageParam, search, DriverId);

                totalRecord = (int)response.TotalRecords;
                filteredRecord = (int)response.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, response.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DriverNotifications_Upsert(DriverNotifications driverNotifications)
        {

            driverNotifications.CreatedBy = ProjectSession.AdminId;

            var result = abstractDriverNotificationsServices.DriverNotifications_Upsert(driverNotifications);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}