﻿using TaxiApp.Common;
using TaxiAppAdmin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
//using TaxiApp.Entities.Contract;
//using TaxiApp.Services.Contract;
//using TaxiApp.Common.Paging;
//using TaxiApp.Entities.V1;

namespace TaxiAppAdmin.Infrastructure
{

    public class BaseController : Controller
    {
        public BaseController()
        {

        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {

                bool isAllow = false;
                HttpCookie reqCookie = Request.Cookies["UserLogin"];
                if (reqCookie != null)
                {
                    ProjectSession.AdminId = Convert.ToInt32(Convert.ToString(reqCookie["Id"]));
                }
                else
                {
                    ProjectSession.AdminId = 0;
                }

                string var_sql = "select * from AdminUsers where Id = " + ProjectSession.AdminId;

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    ProjectSession.AdminName = dt.Rows[0]["FirstName"].ToString() + ' ' + dt.Rows[0]["LastName"].ToString();
                    isAllow = true;
                }
                else
                {
                    isAllow = false;
                }

                if (!isAllow)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/Signin");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Authentication/Signin");
            }
        }

    }
}







    //public class BaseController : Controller
    //{
    //    public BaseController()
    //    {

    //    }

    //    protected override void Initialize(System.Web.Routing.RequestContext requestContext)
    //    {
    //        base.Initialize(requestContext);
    //    }

    //    protected override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        try
    //        {
    //            bool isAllow = false;
    //            HttpCookie reqCookie = Request.Cookies["UsersLogin"];

    //            if (reqCookie != null)
    //            {
    //                ProjectSession.AdminId = Convert.ToInt64(Convert.ToString(reqCookie["Id"]));
    //                ProjectSession.AdminName = Convert.ToString(reqCookie["Name"]);
    //                ProjectSession.UserTypeId = Convert.ToInt64(reqCookie["UserTypeId"]);
    //                ProjectSession.CompanyId = Convert.ToInt64(reqCookie["CompanyId"]);
    //                ProjectSession.Email = Convert.ToString(reqCookie["Email"]);
    //                ProjectSession.UserTypeName = Convert.ToString(reqCookie["UserTypeName"]);
    //                ProjectSession.PagePermission = Convert.ToString(reqCookie["PagePermission"]);

    //                isAllow = true;

    //                if (!ProjectSession.PagePermission.Contains(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName))
    //                {
    //                    isAllow = false;
    //                }
    //                if (ProjectSession.UserTypeId == 1 &&
    //                    (
    //                     filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Players"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Reports"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersConsultsNotifications"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersPlayingHistory"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersIssuesNotifications"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "StaffDashboard"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Dashboard"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersDiary"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersWorkShop"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersConsults"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Issues"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Attachments"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Workshop"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "MasterData"
    //                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Engagement")
    //                    )
    //                {
    //                    isAllow = false;
    //                }
    //                if (ProjectSession.UserTypeId == 2 &&
    //                    (
    //                     filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Company"
    //                    )
    //                    )
    //                {
    //                    isAllow = true;
    //                }
    //                if (ProjectSession.UserTypeId == 3)
    //                {
    //                    if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Company"
    //                        && filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Users"
    //                        //&&  filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "MasterData"
    //                        )
    //                    {
    //                        string[] PagePermissionArry = ProjectSession.PagePermission.Split(',');
    //                        for (int i = 0; i < PagePermissionArry.Length; i++)
    //                        {
    //                            if (PagePermissionArry[i] == "Dashboard"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Players"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Reports"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersConsultsNotifications"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersPlayingHistory"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersIssuesNotifications"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "StaffDashboard"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersDiary"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersWorkShop"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "PlayersConsults"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Issues"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Attachments"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Workshop"
    //                                //|| filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Admin"
    //                                //|| filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "MasterData"
    //                                || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Engagement")
    //                            {
    //                                isAllow = true;
    //                            }
    //                            else
    //                            {
    //                                isAllow = false;
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        isAllow = false;
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                isAllow = false;
    //            }

    //            if (!isAllow)
    //            {
    //                filterContext.Result = new RedirectResult("~/Authentication/Signout");
    //                return;
    //            }

    //            base.OnActionExecuting(filterContext);
    //        }
    //        catch (Exception)
    //        {
    //            filterContext.Result = new RedirectResult("~/Authentication/Signout");
    //        }
    //    }

    //}
