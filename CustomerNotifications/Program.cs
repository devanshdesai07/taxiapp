﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;


namespace CustomerNotifications
{
    internal class Program
    {
        public static string ConnStr = "Data Source=rushkar-db-z.cwfpajxcr0v7.ap-south-1.rds.amazonaws.com;Initial Catalog=TaxiApp;Persist Security Info=True;User ID=sa;Password=*aA123123;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True";
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {
                while (true)
                {
                    string q = "select * from [dbo].[CustomerNotifications] where IsSent = 0";
                    
                    SqlDataAdapter ad = new SqlDataAdapter(q, ConnStr);
                    DataTable dt = new DataTable();


                    ad.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        List<string> DeviceT = new List<string>();
                        Console.WriteLine("Getting Connection ...");

                        foreach (DataRow row in dt.Rows)
                        {
                            if (dt.Rows.Count > 0)
                            {

                                string getDT = "select DeviceToken from [dbo].[Customer] where Id = " + row["CustomerId"].ToString();
                                SqlDataAdapter DT = new SqlDataAdapter(getDT, conn);
                                DataTable DeviceToken = new DataTable();
                                DT.Fill(DeviceToken);

                                if (DeviceToken.Rows.Count > 0)
                                {
                                    DeviceT = DeviceToken.AsEnumerable()
                                                  .Select(r => r.Field<string>("DeviceToken"))
                                                  .ToList();
                                }

                                if (DeviceT.Count > 0)
                                {
                                    SendNotification(DeviceT, row["NotificationJSON"].ToString(), row["id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.Read();
        }
        public static string SendNotification(List<string> DeviceT, string NotificationJSON, string Id)
        {
            string response = string.Empty;
            string serverKey = "AAAA3kYPu_o:APA91bFUeoxvXBpt44yVP16t5uWDh4zXultBR2pex6eImY5rKFtMizs6x_SVi7pcE418KzVgx3pf9yMI2wbBcQQe0_VIQteicGsl-fFUb2CQIEqftoyJhQ8NF8tfarmg9q4Sk4SzMCLe"; // Something very long

            HttpWebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send") as HttpWebRequest;

            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                registration_ids = DeviceT,
                //to = deviceId,
                data = new
                {
                    body = NotificationJSON,
                    title = "",
                    sound = "default",
                    mutable_contenct = true,
                    badge = 4,
                    data = new { }
                },
                mutable_contenct = true
            };
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes((string)json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));

            tRequest.ContentLength = byteArray.Length;
            string q = "";
            string S = "";
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (HttpWebResponse tResponse = tRequest.GetResponse() as HttpWebResponse)
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();

                            SqlConnection Conn = new SqlConnection(ConnStr);
                            Conn.Open();

                            string updateIsSent = "UPDATE CustomerNotifications SET IsSent = 1 where Id = " + Convert.ToInt64(Id);
                            SqlCommand UpdateDENotificationcmd = new SqlCommand(updateIsSent, Conn);
                            UpdateDENotificationcmd.ExecuteNonQuery();

                            Conn.Close();
                            Console.WriteLine("Send successful!");
                        }
                    }
                }
            }
            return "";
        }
    }
}
