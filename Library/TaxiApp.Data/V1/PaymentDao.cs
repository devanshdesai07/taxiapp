﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using Dapper;

namespace TaxiApp.Data.V1
{
  
    public class PaymentDao : AbstractPaymentDao
    {
        
        public override PagedList<AbstractPayment> Payment_All(PageParam pageParam, string Search,int TripId, int DriverId, int CustomerId)
        {
            PagedList<AbstractPayment> Payment = new PagedList<AbstractPayment>();

            var param = new DynamicParameters();
            param.Add("@Offset", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);            
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);            
            param.Add("@TripId", TripId, dbType: DbType.Int32, direction: ParameterDirection.Input);            
            param.Add("@DriverId", DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);            
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_All, param, commandType: CommandType.StoredProcedure);
                Payment.Values.AddRange(task.Read<Payment>());
                Payment.TotalRecords = task.Read<int>().SingleOrDefault();
            }
            return Payment;
        }
      
        public override SuccessResult<AbstractPayment> Payment_ById(int Id)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_ById, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }

            return Payment;
        }
        
       
        public override SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment)
        {
            SuccessResult<AbstractPayment> Payment = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPayment.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId  ", abstractPayment.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TripId  ", abstractPayment.TripId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Amount  ", abstractPayment.Amount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@IsPay  ", abstractPayment.IsPay, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPayment.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPayment.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Payment_Upsert, param, commandType: CommandType.StoredProcedure);
                Payment = task.Read<SuccessResult<AbstractPayment>>().SingleOrDefault();
                Payment.Item = task.Read<Payment>().SingleOrDefault();
            }

            return Payment;
        }



    }
}
