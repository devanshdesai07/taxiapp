﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using Dapper;

namespace TaxiApp.Data.V1
{
    public class LiveLatLongOfDriverCustomerDao : AbstractLiveLatLongOfDriverCustomerDao
    {

        public override PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_driverId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer)
        {
            PagedList<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriverCustomer = new PagedList<AbstractLiveLatLongOfDriverCustomer>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractLiveLatLongOfDriverCustomer.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TripId", abstractLiveLatLongOfDriverCustomer.TripId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.livelatitude_By_driverId, param, commandType: CommandType.StoredProcedure);
                LiveLatLongOfDriverCustomer.Values.AddRange(task.Read<LiveLatLongOfDriverCustomer>());
                LiveLatLongOfDriverCustomer.TotalRecords = task.Read<int>().SingleOrDefault();
            }
            return LiveLatLongOfDriverCustomer;
        }

        public override PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_CustomerId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer)
        {
            PagedList<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriverCustomer = new PagedList<AbstractLiveLatLongOfDriverCustomer>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractLiveLatLongOfDriverCustomer.CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TripId", abstractLiveLatLongOfDriverCustomer.TripId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.livelatitude_By_CustomerId, param, commandType: CommandType.StoredProcedure);
                LiveLatLongOfDriverCustomer.Values.AddRange(task.Read<LiveLatLongOfDriverCustomer>());
                LiveLatLongOfDriverCustomer.TotalRecords = task.Read<int>().SingleOrDefault();
            }
            return LiveLatLongOfDriverCustomer;
        }




        //public override SuccessResult<AbstractTrip> Trip_ById(int Id)
        //{
        //    SuccessResult<AbstractTrip> Trip = null;
        //    var param = new DynamicParameters();

        //    param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.Trip_ById, param, commandType: CommandType.StoredProcedure);
        //        Trip = task.Read<SuccessResult<AbstractTrip>>().SingleOrDefault();
        //        Trip.Item = task.Read<Trip>().SingleOrDefault();
        //        Trip.Item.ExpectedPriceDetails = task.Read<TripPrice>().FirstOrDefault();
        //        Trip.Item.MasterNotes.AddRange(task.Read<TripNotes>());
        //        Trip.Item.DriverDetails = task.Read<Driver>().FirstOrDefault();
        //        Trip.Item.CustomerDetails = task.Read<Customer>().FirstOrDefault();

        //    }

        //    return Trip;
        //}



        //public override SuccessResult<AbstractTrip> Trip_Delete(int Id, int DeletedBy)
        //{
        //    SuccessResult<AbstractTrip> Trip = null;
        //    var param = new DynamicParameters();

        //    param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
        //    param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple(SQLConfig.Trip_Delete, param, commandType: CommandType.StoredProcedure);
        //        Trip = task.Read<SuccessResult<AbstractTrip>>().SingleOrDefault();
        //        Trip.Item = task.Read<Trip>().SingleOrDefault();
        //    }
        //    return Trip;
        //}    



        public override SuccessResult<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriver_Upsert(AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer)
        {
            SuccessResult<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriverCustomer = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLiveLatLongOfDriverCustomer.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractLiveLatLongOfDriverCustomer.CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TripId", abstractLiveLatLongOfDriverCustomer.TripId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractLiveLatLongOfDriverCustomer.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@LivePickUpLat", abstractLiveLatLongOfDriverCustomer.LivePickUpLat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LivePickUpLong", abstractLiveLatLongOfDriverCustomer.LivePickUpLong, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LiveDropOffLat", abstractLiveLatLongOfDriverCustomer.LiveDropOffLat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LiveDropOffLong", abstractLiveLatLongOfDriverCustomer.LiveDropOffLong, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LiveLatLongOfDriver_Upsert, param, commandType: CommandType.StoredProcedure);
                LiveLatLongOfDriverCustomer = task.Read<SuccessResult<AbstractLiveLatLongOfDriverCustomer>>().SingleOrDefault();
                LiveLatLongOfDriverCustomer.Item = task.Read<LiveLatLongOfDriverCustomer>().SingleOrDefault();
            }

            return LiveLatLongOfDriverCustomer;
        }
       


    }
}
