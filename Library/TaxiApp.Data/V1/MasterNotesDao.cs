﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Entities.V1;
using Dapper;

namespace TaxiApp.Data.V1
{
    public class MasterNotesDao : AbstractMasterNotesDao
    {



        public override PagedList<AbstractMasterNotes> MasterNotes_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterNotes> MasterNotes = new PagedList<AbstractMasterNotes>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterNotes_All, param, commandType: CommandType.StoredProcedure);
                MasterNotes.Values.AddRange(task.Read<MasterNotes>());
                MasterNotes.TotalRecords = task.Read<int>().SingleOrDefault();
            }
            return MasterNotes;
        }
        public override SuccessResult<AbstractMasterNotes> MasterNotes_ById(int Id)
        {
            SuccessResult<AbstractMasterNotes> MasterNotes = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterNotes_ById, param, commandType: CommandType.StoredProcedure);
                MasterNotes = task.Read<SuccessResult<AbstractMasterNotes>>().SingleOrDefault();
                MasterNotes.Item = task.Read<MasterNotes>().SingleOrDefault();




            }

            return MasterNotes;
        }
        
        
        

        public override SuccessResult<AbstractMasterNotes> MasterNotes_Upsert(AbstractMasterNotes abstractMasterNotes)
        {
            SuccessResult<AbstractMasterNotes> MasterNotes = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterNotes.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Notes", abstractMasterNotes.Notes, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractMasterNotes.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractMasterNotes.UpdatedBy, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterNotes_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterNotes = task.Read<SuccessResult<AbstractMasterNotes>>().SingleOrDefault();
                MasterNotes.Item = task.Read<MasterNotes>().SingleOrDefault();
            }

            return MasterNotes;
        }

        public override SuccessResult<AbstractMasterNotes> MasterNotes_Delete(int Id,int DeletedBy)
        {
            SuccessResult<AbstractMasterNotes> MasterNotes = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterNotes_Delete, param, commandType: CommandType.StoredProcedure);
                MasterNotes = task.Read<SuccessResult<AbstractMasterNotes>>().SingleOrDefault();
                MasterNotes.Item = task.Read<MasterNotes>().SingleOrDefault();
            }
            return MasterNotes;
        }
    }
}
