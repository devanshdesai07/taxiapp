﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;

namespace TaxiApp.Data.Contract
{
    public abstract class AbstractLiveLatLongOfDriverCustomerDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_CustomerId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer);
        public abstract PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_driverId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer);
        // public abstract SuccessResult<AbstractTrip> Trip_ById(int Id);
        //public abstract SuccessResult<AbstractTrip> Trip_Delete(int Id, int DeletedBy);
        public abstract SuccessResult<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriver_Upsert(AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer);
       
     

    }
}
