﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;

namespace TaxiApp.Data.Contract
{
    public abstract class AbstractMasterNotesDao : AbstractBaseDao
    {
        public abstract PagedList<AbstractMasterNotes> MasterNotes_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractMasterNotes> MasterNotes_ById(int Id);
        public abstract SuccessResult<AbstractMasterNotes> MasterNotes_Upsert(AbstractMasterNotes AbstractMasterNotes);

        public abstract SuccessResult<AbstractMasterNotes> MasterNotes_Delete(int Id,int DeletedBy);


    }
}
