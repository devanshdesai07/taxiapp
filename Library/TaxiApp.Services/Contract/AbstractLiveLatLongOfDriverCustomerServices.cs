﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;

namespace TaxiApp.Services.Contract
{
    public abstract class AbstractLiveLatLongOfDriverCustomerServices
    {
        public abstract SuccessResult<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriver_Upsert(AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer);
        public abstract PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_driverId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer);
          public abstract PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_CustomerId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer);


    }
}
