﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;

namespace TaxiApp.Services.Contract
{
    public abstract class AbstractCustomerServices
    {
        public abstract PagedList<AbstractCustomer> Customer_All(PageParam pageParam, string search, AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_ById(int Id);
        public abstract SuccessResult<AbstractCustomer> Customer_ByEmail(string Email);

        public abstract SuccessResult<AbstractCustomer> Trip_Start(int CustomerId);
        public abstract SuccessResult<AbstractCustomer> Customer_Delete(int Id, int DeletedBy);
        public abstract SuccessResult<AbstractCustomer> Customer_RcCardUpdate(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_AddressUpdate(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_IdProofUpdate(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_ProfilePictureUpdate(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_Login(string Email, string Password, string DeviceToken);
        //public abstract SuccessResult<AbstractCustomer> Customer_ChangePassword(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_ChangePassword(int Id, string OldPassword, string NewPassword, string ConfirmPassword, string MobileNumber, int Type);
        public abstract SuccessResult<AbstractCustomer> Customer_Upsert(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> Customer_ActInAct(int Id);
        public abstract SuccessResult<AbstractCustomer> Customer_RcCardApproved(int Id);
        public abstract SuccessResult<AbstractCustomer> Customer_IdProofApproved(int Id);
        public abstract bool Customer_Logout(int Id);

    }
}
