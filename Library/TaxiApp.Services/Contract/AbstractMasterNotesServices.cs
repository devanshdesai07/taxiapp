﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;

namespace TaxiApp.Services.Contract
{
    public abstract class AbstractMasterNotesServices
    {
        public abstract PagedList<AbstractMasterNotes> MasterNotes_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractMasterNotes> MasterNotes_ById(int Id);
        public abstract SuccessResult<AbstractMasterNotes> MasterNotes_Upsert(AbstractMasterNotes abstractMasterNotes);

        public abstract SuccessResult<AbstractMasterNotes> MasterNotes_Delete(int Id,int DeletedBy);
    }
}
