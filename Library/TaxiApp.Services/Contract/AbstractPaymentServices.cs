﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Entities.Contract;

namespace TaxiApp.Services.Contract
{
    public abstract class AbstractPaymentServices
    {
        public abstract PagedList<AbstractPayment> Payment_All(PageParam pageParam, string Search, int TripId, int DriverId, int CustomerId);        
      //  public abstract bool Payment_Delete(int Id);
        public abstract SuccessResult<AbstractPayment> Payment_ById(int Id);
        public abstract SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment);
                
    }
}
