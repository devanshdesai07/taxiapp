﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;

namespace TaxiApp.Services.V1
{
    public class LiveLatLongOfDriverCustomerServices : AbstractLiveLatLongOfDriverCustomerServices
    {
        private AbstractLiveLatLongOfDriverCustomerDao abstractLiveLatLongOfDriverCustomerDao;

        public LiveLatLongOfDriverCustomerServices(AbstractLiveLatLongOfDriverCustomerDao abstractLiveLatLongOfDriverCustomerDao)
        {
            this.abstractLiveLatLongOfDriverCustomerDao = abstractLiveLatLongOfDriverCustomerDao;
        }

        public override PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_CustomerId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer)
        {
            return this.abstractLiveLatLongOfDriverCustomerDao.livelatitude_By_CustomerId(pageParam, search, abstractLiveLatLongOfDriverCustomer);
        }
        public override PagedList<AbstractLiveLatLongOfDriverCustomer> livelatitude_By_driverId(PageParam pageParam, string search, AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer)
        {
            return this.abstractLiveLatLongOfDriverCustomerDao.livelatitude_By_driverId(pageParam, search, abstractLiveLatLongOfDriverCustomer);
        }

        //public override SuccessResult<AbstractTrip> Trip_ById(int Id)
        //{
        //    return this.abstractTripDao.Trip_ById(Id);
        //}

        public override SuccessResult<AbstractLiveLatLongOfDriverCustomer> LiveLatLongOfDriver_Upsert(AbstractLiveLatLongOfDriverCustomer abstractLiveLatLongOfDriverCustomer)
        {
            return this.abstractLiveLatLongOfDriverCustomerDao.LiveLatLongOfDriver_Upsert(abstractLiveLatLongOfDriverCustomer);
        }
        //public override SuccessResult<AbstractTrip> Trip_Delete(int Id, int DeletedBy)
        //{
        //    return this.abstractTripDao.Trip_Delete(Id, DeletedBy);
        //}


    }

}