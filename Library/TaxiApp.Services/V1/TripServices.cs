﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;

namespace TaxiApp.Services.V1
{
    public class TripServices : AbstractTripServices
    {
        private AbstractTripDao abstractTripDao;

        public TripServices(AbstractTripDao abstractTripDao)
        {
            this.abstractTripDao = abstractTripDao;
        }

        public override PagedList<AbstractTrip> Trip_All(PageParam pageParam, string search , AbstractTrip abstractTrip, string TripStatusIds)
        {
            return this.abstractTripDao.Trip_All(pageParam, search, abstractTrip, TripStatusIds);
        }    
        public override PagedList<AbstractTrip> Driver_TripAllDetail(PageParam pageParam, string search, int DriverId, int TripStatusId)
        {
            return this.abstractTripDao.Driver_TripAllDetail(pageParam, search, DriverId,TripStatusId);
        }   
        public override PagedList<AbstractTrip> Customer_TripAllDetail(PageParam pageParam, string search, int CustomerId, int TripStatusId)
        {
            return this.abstractTripDao.Customer_TripAllDetail(pageParam, search, CustomerId,TripStatusId);
        } 
        public override PagedList<AbstractTrip> DriverRejectedTrip_All(PageParam pageParam, string search , AbstractTrip abstractTrip)
        {
            return this.abstractTripDao.DriverRejectedTrip_All(pageParam, search, abstractTrip);
        }
        public override PagedList<AbstractTrip> TripDetails_ByCustomerId(PageParam pageParam, string search, AbstractTrip abstractTrip)
        {
            return this.abstractTripDao.TripDetails_ByCustomerId(pageParam, search, abstractTrip);
        }

        public override SuccessResult<AbstractTrip> Trip_ById(int Id)
        {
            return this.abstractTripDao.Trip_ById(Id);
        }
        public override SuccessResult<AbstractTrip> TripRating_Upsert(int id, int Rating)
        {
            return this.abstractTripDao.TripRating_Upsert(id,Rating);
        }
        public override SuccessResult<AbstractTrip> PromoCode_Apply(int TripId, string Promocode)
        {
            return this.abstractTripDao.PromoCode_Apply(TripId,Promocode);
        }
        public override SuccessResult<AbstractTrip> PromoCode_Remove(int TripId)
        {
            return this.abstractTripDao.PromoCode_Remove(TripId);
        }
        public override SuccessResult<AbstractTrip> Trip_SendOtp(string MobileNo, int Otp)
        {
            return this.abstractTripDao.Trip_SendOtp(MobileNo, Otp);
        }
        public override SuccessResult<AbstractTrip> Trip_VerifyOtp(string MobileNo, int Otp)
        {
            return this.abstractTripDao.Trip_VerifyOtp(MobileNo, Otp);
        }

        public override SuccessResult<AbstractTrip> Trip_Upsert(AbstractTrip abstractTrip)
        {
            return this.abstractTripDao.Trip_Upsert(abstractTrip); 
        }

        public override SuccessResult<AbstractTrip> TripTempAssignDriver_Upsert(int Id, int TripId)
        {
            return this.abstractTripDao.TripTempAssignDriver_Upsert(Id, TripId);
        } 
        public override SuccessResult<AbstractTrip> Trip_Delete(int Id, int DeletedBy)
        {
            return this.abstractTripDao.Trip_Delete(Id, DeletedBy);
        }
        public override SuccessResult<AbstractTrip> Driver_Assigned(AbstractTrip abstractTrip)
        {
            return this.abstractTripDao.Driver_Assigned(abstractTrip);
        }
        public override SuccessResult<AbstractTrip> Trip_AcceptByDriver(int DriverId, int TripId,string Polyline1 )
        {
            return this.abstractTripDao.Trip_AcceptByDriver(DriverId, TripId, Polyline1);
        } 
        public override SuccessResult<AbstractTrip> Trip_Begin(int TripId, int DriverId, int Otp, string Polyline2,DateTime BeginTripTime ,DateTime BeginTripDate )
        {
            return this.abstractTripDao.Trip_Begin(TripId, DriverId, Otp,Polyline2,BeginTripTime,BeginTripDate);
        }
        public override SuccessResult<AbstractTrip> Trip_End(int DriverId, int TripId, DateTime EndTripTime, DateTime EndTripDate, string EndTripActualHour, string EndTripHour )
        {
            return this.abstractTripDao.Trip_End(DriverId, TripId,EndTripTime,EndTripDate,EndTripActualHour,EndTripHour);
        }

        public override SuccessResult<AbstractTrip> Trip_StartbyDriver(int DriverId, int TripId, string DriverLocation, string CustomerPickUpLocation)
        {
            return this.abstractTripDao.Trip_StartbyDriver(DriverId, TripId, DriverLocation, CustomerPickUpLocation);
        }
        public override SuccessResult<AbstractTrip> Trip_RejectByDriver(int DriverId, int TripId)
        {
            return this.abstractTripDao.Trip_RejectByDriver(DriverId, TripId);
        }
        public override SuccessResult<AbstractTrip> Trip_CancelByCustomer(int TripId, int MasterTripCancelReasonId)
        {
            return this.abstractTripDao.Trip_CancelByCustomer(TripId ,MasterTripCancelReasonId);
        }
        public override PagedList<AbstractTrip> TripList_ApprovalPending(PageParam pageParam, AbstractTrip abstractTrip)
        {
            return this.abstractTripDao.TripList_ApprovalPending(pageParam, abstractTrip);
        }
    }

}