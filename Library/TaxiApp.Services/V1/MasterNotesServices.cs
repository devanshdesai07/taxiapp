﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;

namespace TaxiApp.Services.V1
{
    public class MasterNotesServices : AbstractMasterNotesServices
    {
        private AbstractMasterNotesDao abstractMasterNotesDao;

        public MasterNotesServices(AbstractMasterNotesDao abstractMasterNotesDao)
        {
            this.abstractMasterNotesDao = abstractMasterNotesDao;
        }

       
        public override PagedList<AbstractMasterNotes> MasterNotes_All(PageParam pageParam, string search)
        {
            return this.abstractMasterNotesDao.MasterNotes_All(pageParam, search);
        }

        public override SuccessResult<AbstractMasterNotes> MasterNotes_ById(int Id)
        {
            return this.abstractMasterNotesDao.MasterNotes_ById(Id);
        }

        public override SuccessResult<AbstractMasterNotes> MasterNotes_Upsert(AbstractMasterNotes abstractMasterNotes)
        {
            return this.abstractMasterNotesDao.MasterNotes_Upsert(abstractMasterNotes);
        }

        public override SuccessResult<AbstractMasterNotes> MasterNotes_Delete(int Id,int DeletedBy)
        {
            return this.abstractMasterNotesDao.MasterNotes_Delete(Id, DeletedBy);
        }



    }

}