﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;

namespace TaxiApp.Services.V1
{
    public class PaymentServices : AbstractPaymentServices
    {
        private AbstractPaymentDao abstractPaymentDao;

        public PaymentServices(AbstractPaymentDao abstractPaymentDao)
        {
            this.abstractPaymentDao = abstractPaymentDao;
        }

        public override SuccessResult<AbstractPayment> Payment_ById(int Id)
        {
            return this.abstractPaymentDao.Payment_ById(Id);
        }
        public override PagedList<AbstractPayment> Payment_All(PageParam pageParam, string Search, int TripId, int DriverId, int CustomerId)
        {
            return this.abstractPaymentDao.Payment_All(pageParam, Search, TripId, DriverId, CustomerId);
        }
              
        public override SuccessResult<AbstractPayment> Payment_Upsert(AbstractPayment abstractPayment)
        {
            return this.abstractPaymentDao.Payment_Upsert(abstractPayment);
        }               
        //public override bool Payment_Delete(int Id)
        //{
        //    return this.abstractPaymentDao.Payment_Delete(Id);
        //}       
    }

}