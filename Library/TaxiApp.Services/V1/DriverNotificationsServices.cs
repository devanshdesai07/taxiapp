﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;
using TaxiApp.Common.Paging;
using TaxiApp.Data.Contract;
using TaxiApp.Entities.Contract;
using TaxiApp.Services.Contract;

namespace TaxiApp.Services.V1
{
    public class DriverNotificationsServices : AbstractDriverNotificationsServices
    {
        private AbstractDriverNotificationsDao abstractDriverNotificationsDao;

        public DriverNotificationsServices(AbstractDriverNotificationsDao abstractDriverNotificationsDao)
        {
            this.abstractDriverNotificationsDao = abstractDriverNotificationsDao;
        }


        public override PagedList<AbstractDriverNotifications> DriverNotifications_All(PageParam pageParam, string search, int driverId)
        {
            return this.abstractDriverNotificationsDao.DriverNotifications_All(pageParam, search, driverId);
        }

        public override SuccessResult<AbstractDriverNotifications> DriverNotifications_ById(int Id)
        {
            return this.abstractDriverNotificationsDao.DriverNotifications_ById(Id);
        }

        public override SuccessResult<AbstractDriverNotifications> DriverNotifications_Upsert(AbstractDriverNotifications abstractDriverNotifications)
        {
            return this.abstractDriverNotificationsDao.DriverNotifications_Upsert(abstractDriverNotifications);
        }

        public override SuccessResult<AbstractDriverNotifications> DriverNotifications_ReadUnRead(int Id)
        {
            return this.abstractDriverNotificationsDao.DriverNotifications_ReadUnRead(Id);
        }

        public override SuccessResult<AbstractDriverNotifications> DriverNotifications_Delete(int Id)
        {
            return this.abstractDriverNotificationsDao.DriverNotifications_Delete(Id);
        }

        public override PagedList<AbstractDriverNotifications> DriverSendNotifications_ByDriverIdReadUnRead(PageParam pageParam, long DriverId)
        {
            return this.abstractDriverNotificationsDao.DriverSendNotifications_ByDriverIdReadUnRead(pageParam, DriverId);
        }

        public override PagedList<AbstractDriverNotifications> DriverSendNotifications_Dropdown(PageParam pageParam)
        {
            return this.abstractDriverNotificationsDao.DriverSendNotifications_Dropdown(pageParam);

        }
        public override PagedList<AbstractDriverNotifications> DriverSendNotifications_ByDriverId(PageParam pageParam, long DriverId)
        {
            return this.abstractDriverNotificationsDao.DriverSendNotifications_ByDriverId(pageParam, DriverId);
        }
    }
}