﻿using TaxiApp;
using TaxiApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;

namespace TaxiApp.Entities.Contract
{
    public abstract class AbstractLiveLatLongOfDriverCustomer
    {
        public int Id { get; set; }
        public int TripId { get; set; }
        public int CustomerId { get; set; }
        public int DriverId { get; set; }
        public string LiveDropOffLong { get; set; }
        public string LiveDropOffLat { get; set; }
        public string LivePickUpLong { get; set; }
        public string LivePickUpLat { get; set; }
    }
}

