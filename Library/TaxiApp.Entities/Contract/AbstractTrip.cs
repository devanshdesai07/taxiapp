﻿using TaxiApp;
using TaxiApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiApp.Common;

namespace TaxiApp.Entities.Contract
{
    public abstract class AbstractTrip
    {
        public decimal DriverCommision { get; set; }
        public decimal FinalAmount { get; set; }
        public int Id { get; set; }
        public int MasterTripCancelReasonId { get; set; }
        public int TripId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerDeviceToken { get; set; }
        public string Reason { get; set; }
        public string DriverLocation { get; set; }
        public string CustomerPickUpLocation { get; set; }
        public int DriverId { get; set; }
        public int TotalTrip { get; set; }
        public decimal TotalDriverRating { get; set; }
        public decimal AverageRating { get; set; }
       
        public string AssignedDriverName { get; set; }
        public string CustomerName { get; set; }
        public string AssignedDriverMobileNo { get; set; }
        public string AssignedDriverVehicleNo { get; set; }
        public int AssignedId { get; set; }
        public int MasterServiceBaseId { get; set; }
        public int MasterTripTypeId { get; set; }
        public DateTime DropOffDate { get; set; }
        public DateTime DropOffTime { get; set; }
        public DateTime BeginTripDate { get; set; }
        public DateTime BeginTripTime { get; set; }
        public DateTime EndTripTime { get; set; }
        public DateTime EndTripDate { get; set; }
        public string TotalKiloMeters { get; set; }

        //public string OTP { get; set; }AssignedDriverName
        public int Otp { get; set; }
        public string PickUpLat { get; set; }
        public string DropOffLat { get; set; }
        public string PickUpLong { get; set; }
        public string DropOffLong { get; set; }
        public int CustomerTripOTP { get; set; }

        public DateTime PickUpDate { get; set; }
        public DateTime PickUpTime { get; set; }
        public int NoOfPassengers { get; set; }
        public int TripStatusId { get; set; }
        public string TripStatusIds { get; set; }
        public string PickUpCity { get; set; }
        public string PickUpState { get; set; }
        public string TripStatusName { get; set; }
        public string CustomerGender { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerProfilePicture { get; set; }

        public int PickUpPinCode { get; set; }
        public bool Isroundtrip { get; set; }
        public string PickUpAddress { get; set; }
        public int PickUpPhoneNumber { get; set; }
        public string DropOffCity { get; set; }
        public string DropOffState { get; set; }
        public int DropOffPinCode { get; set; }
        public string DropOffAddress { get; set; }
        public int DropOffPhoneNumber { get; set; }

        public bool IsCancelled { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerCountry { get; set; }
        public string TotalTimeInHours { get; set; }
        public string ActualTimeInHours { get; set; }
        public string CustomerPinCode { get; set; }
        public string CustomerMobileNo { get; set; }
        public string AssignedDriverGender { get; set; }
        public string CustomerState { get; set; }
        public bool IsDeleted { get; set; }
        public int DeletedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public DateTime TripCancelledDate { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CustomerDOB { get; set; }
        public string EndTripActualHour { get; set; }
        public string EndTripHour { get; set; }
        public string TripDateFrom { get; set; }
        public string Promocode { get; set; }
        public string TotalKiloMetres { get; set; }
        public string AssignedDriverEmail { get; set; }
        public string TripDateTo { get; set; }
        public string TripTimeFrom { get; set; }
        public string TripTimeTo { get; set; }
        public string TotalTime { get; set; }
        public string Polyline1 { get; set; }
        public string Polyline2 { get; set; }
        public string TripRemarkByCustomer { get; set; }
        public decimal TripRatingByCustomer { get; set; }
        public string AssignedDriverProfilePicture { get; set; }
        public decimal TripRatingByDriver { get; set; }
        public decimal DriverRatings { get; set; }
       
        [NotMapped]
        public string DriverProfilePictureStr => Configurations.NewApiUrl + AssignedDriverProfilePicture;

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";       
        [NotMapped]
        public string TripCancelledDateStr => TripCancelledDate != null ? TripCancelledDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string PickUpDateStr => PickUpDate != null ? PickUpDate.ToString("dd-MMM-yyyy") : "-"; 

        [NotMapped]
        public string PickUpTimeStr => PickUpTime != null ? PickUpTime.ToString("hh:mm tt") : "-";
        [NotMapped]
        public string DropOffDateStr => DropOffDate != null ? DropOffDate.ToString("dd-MMM-yyyy") : "-";
        [NotMapped]
        public string DropOffTimeStr => DropOffTime != null ? DropOffTime.ToString("hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string DeletedDateStr => DeletedDate != null ? DeletedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        public List<TripNotes> MasterNotes = new List<TripNotes>();
        public TripPrice ExpectedPriceDetails  = new TripPrice();
        public Driver DriverDetails  = new Driver();
        public Customer CustomerDetails  = new Customer();

     
        
        
    }
    public class TripNotes
    {
        public int Id { get; set; }
        public string Notes { get; set; }
    }  
      

    
    public class TripPrice
    {
        public decimal Price { get; set; }
        public decimal FinalAmount { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal ConveyanceCharge { get; set; }
        public decimal GST { get; set; }
        public decimal NightCharge { get; set; }
        
    }



}

