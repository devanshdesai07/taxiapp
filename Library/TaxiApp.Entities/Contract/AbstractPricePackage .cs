﻿using TaxiApp;
using TaxiApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace TaxiApp.Entities.Contract
{
    public abstract class AbstractPricePackage
    {
        public int Id { get; set; }
        public string PackageName { get; set; }
        public int Hour { get; set; }
        public bool IsOther { get; set; }
        public decimal Price { get;set;}
        public decimal IsPercentage { get;set;}
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public int UpdatedBy { get; set; }
        //public string MasterHourDetails { get; set; }
        public string userEmail { get; set; }


        
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdateDate != null ? UpdateDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";


    }
}

