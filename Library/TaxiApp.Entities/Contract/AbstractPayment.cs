﻿using TaxiApp;
using TaxiApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace TaxiApp.Entities.Contract
{
    public abstract class AbstractPayment
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public bool IsPay { get; set; }
        public int CustomerId { get; set; }
        public decimal DriverCommision { get; set; }
        public int TripId { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalPendingAmount { get; set; }
        public decimal TotalAmountReceived { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public int DeletedBy { get; set; }
        [NotMapped]
        public string TransactionDateStr => TransactionDate != null ? TransactionDate.ToString("dd-MM-yyyy") : "";
        [NotMapped]
        public string TransactionTimeStr => TransactionDate != null ? TransactionDate.ToString("hh:mm tt") : "";

    }
}

